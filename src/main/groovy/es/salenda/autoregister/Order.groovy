package es.salenda.autoregister

class Order {

    List<OrderLine> lines = new ArrayList<OrderLine>()

    def getProducts() {
        lines*.product.unique()
    }

    def getTotal() {
        lines.collect {it.total}.sum() ?: 0
    }

    def add(quantity, product) {
        lines << new OrderLine(product: product, quantity: quantity)
    }

    String toString() {
        if (lines) {
            def result = new StringBuilder()
            result << "Qty. Product                 Price Subtotal\n"
            result << "---- -------------------- -------- --------\n"
            lines.each { result << it }
            def totalPrice = " ${String.format('%.2f', total)} ".padLeft(38, '.')
            result << "TOTAL ${totalPrice}"
        } else {
            ""
        }
    }

    def methodMissing(String name, args) {
        add(args.first(), name as Product)
    }
}