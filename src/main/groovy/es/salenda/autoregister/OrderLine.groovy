package es.salenda.autoregister

class OrderLine {
    Integer quantity
    Product product

    def getTotal() {
        quantity * product.price
    }

    String toString() {
        def productQuantity = "${quantity}".center(4)
        def productName = product.name().padRight(20)
        def productPrice = "${String.format('%.2f', product.price)}".padLeft(8)
        def lineTotal = String.format('%.2f', total).padLeft(8)
        "${productQuantity} ${productName} ${productPrice} ${lineTotal}\n"
    }
}
