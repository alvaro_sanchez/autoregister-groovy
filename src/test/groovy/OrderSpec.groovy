import es.salenda.autoregister.Order
import spock.lang.Specification
import es.salenda.autoregister.Product
import es.salenda.autoregister.OrderLine

class OrderSpec extends Specification{

    def setupSpec() {
        Product.values().each {product ->
            Integer.metaClass."get${product.name()}"() {
                new OrderLine(quantity: delegate, product: product)
            }
        }
    }

    def "orders can be created"() {
        setup:
        def order = new Order()

        expect:
        order
    }

    def "products can be added to order"() {
        expect:
        order.products == products

        where:
        order       | products
        order1      | [Product.WASH]
        order2      | [Product.WASH, Product.CARPET_CLEANING]
        order3      | []
    }

    def "total is calculated in an order"() {
        expect:
        order.total == total

        where:
        order       | total
        order1      | 19.95
        order2      | 89.8
        order3      | 0
    }

    def "receipts are printed"() {
        expect:
        order.toString() == receipt

        where:
        order       | receipt
        order1      | receipt1
        order2      | receipt2
        order3      | receipt3
    }

    def "orders can be built using DSL"() {
        setup:
        def order1 = new Order()
        def order2 = new Order()

        when:
        order1.with {
            WASH 2
        }
        order2.with {
            CARPET_CLEANING 1
            VACUUMING 4
        }

        then:
        order1.products == [Product.WASH]
        order2.products == [Product.CARPET_CLEANING, Product.VACUUMING]
    }

    def "price can be also calculated using DSL"() {
        setup:
        def order1 = new Order()
        def order2 = new Order()

        when:
        order1.with {
            VACUUMING 3
        }
        order2.with {
            CARPET_CLEANING 2
            WASH 3
        }

        then:
        order1.total == 29.97
        order2.total == 71.83

    }

    def getOrder1() {
        def order = new Order()
        order.add(1, Product.WASH)
        order
    }

    def getReceipt1() {
        """\
Qty. Product                 Price Subtotal
---- -------------------- -------- --------
 1   WASH                    19,95    19,95
TOTAL ............................... 19,95 """
    }

    def getReceipt2() {
        """\
Qty. Product                 Price Subtotal
---- -------------------- -------- --------
 3   WASH                    19,95    59,85
 5   CARPET_CLEANING          5,99    29,95
TOTAL ............................... 89,80 """
    }

    def getReceipt3() {
        ""
    }

    def getOrder2() {
        def order = new Order()
        order.add(3, Product.WASH)
        order.add(5, Product.CARPET_CLEANING)
        order
    }

    def getOrder3() {
        new Order()
    }

}
